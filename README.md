# entraide_gen.py

Générer la liste des services du site https://entraide.chatons.org en utilisant les fichiers json hébergé par chaque chatons.

## Usage

```bash
$ ./entraide_join.py -h
usage: entraide_join.py [-h] --liste LISTE --output OUTPUT

optional arguments:
  -h, --help       show this help message and exit
  --liste LISTE    JSON file with list of kitten and their service list url
  --output OUTPUT  JSON output file for entraide.chatons.org website
```

## Exemple de liste

```json
{ "chatons":
        [
                {
                        "name": "Chatons1",
                        "url": "https://notresite.org/entraide.json"
                },
                {
                        "name": "Kitten",
                        "url": "https://kitten.co.uk/myservices.json"
                },
                {
                        "name": "éhéhé",
                        "url": "https://éhéhé.org/NotreCarteIllustrée.json"
                }
        ]
}
```
Le script prend en charge les [IDN](https://wikipedia.org/IDN)


## Example

```bash
$ python entraide_join.py --liste liste_example.json --output chatons-service.json | jq .
```
```json
{
  "nodes": [
    {
      "node": {
        "degradability": "Jamais",
        "open": "Ouvert à tou⋅te⋅s sans inscription",
        "type": "Aide à la prise de rendez-vous (alternative à Doodle)",
        "software": "Framadate",
        "consent": "1",
        "chaton_website": "https://www.nomagic.uk",
        "chaton": "Nomagic",
        "weight": "1",
        "edit_link": "https://chatons.org/fr/node/3251/edit",
        "endpoint": "https://framadate.nomagic.uk"
      }
    },
    {
      "node": {
        "degradability": "Jamais",
        "open": "Ouvert à tou⋅te⋅s sans inscription",
        "type": "Aide à la prise de rendez-vous (alternative à Doodle)",
        "software": "Truc",
        "consent": "1",
        "chaton_website": "https://www.nomagic.uk",
        "chaton": "Nomagic",
        "weight": "1",
        "endpoint": "https://truc.nomagic.uk"
      }
    },
    {
      "node": {
        "degradability": "Jamais",
        "open": "Ouvert à tou⋅te⋅s sans inscription",
        "type": "Aide à la prise de rendez-vous (alternative à Doodle)",
        "software": "Framadate",
        "consent": "1",
        "chaton_website": "https://www.nomagic.uk",
        "chaton": "Nomagic",
        "weight": "1",
        "edit_link": "https://chatons.org/fr/node/3251/edit",
        "endpoint": "https://framadate.nomagic.uk"
      }
    },
    {
      "node": {
        "degradability": "Jamais",
        "open": "Ouvert à tou⋅te⋅s sans inscription",
        "type": "Aide à la prise de rendez-vous (alternative à Doodle)",
        "software": "Truc",
        "consent": "1",
        "chaton_website": "https://www.nomagic.uk",
        "chaton": "Nomagic",
        "weight": "1",
        "endpoint": "https://truc.nomagic.uk"
      }
    },
    {
      "node": {
        "degradability": "Jamais",
        "open": "Ouvert à tou⋅te⋅s sans inscription",
        "type": "Aide à la prise de rendez-vous (alternative à Doodle)",
        "software": "Framadate",
        "consent": "1",
        "chaton_website": "https://www.nomagic.uk",
        "chaton": "Nomagic",
        "weight": "1",
        "edit_link": "https://chatons.org/fr/node/3251/edit",
        "endpoint": "https://framadate.nomagic.uk"
      }
    },
    {
      "node": {
        "degradability": "Jamais",
        "open": "Ouvert à tou⋅te⋅s sans inscription",
        "type": "Aide à la prise de rendez-vous (alternative à Doodle)",
        "software": "Truc",
        "consent": "1",
        "chaton_website": "https://www.nomagic.uk",
        "chaton": "Nomagic",
        "weight": "1",
        "endpoint": "https://truc.nomagic.uk"
      }
    }
  ]
}
```