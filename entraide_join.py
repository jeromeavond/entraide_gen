#!/usr/bin/env python
# coding: utf-8

import urllib.request as req
import json,ssl
from url_normalize import url_normalize
import argparse

debug=True

parser = argparse.ArgumentParser()
parser.add_argument('--liste',type=argparse.FileType('r'),required=True, help="JSON file with list of kitten and their service list url")
parser.add_argument('--output',type=argparse.FileType('w'),required=True, help="JSON output file for entraide.chatons.org website")
args=parser.parse_args()

listfile=args.liste.name
#
#   Liste de chatons :
#   fichier json
#   { "chatons" : 
#     [
#       { 
#         "name": "MonChatons",
#         "url": "https://monchatons.org/entraide.json"
#       },
#       { 
#         "name": "...",
#         "url": "https://..."
#       }, 
#       ... 
#     ]
#   }
# 
jsonOutput=args.output.name
#
#   Fichier JSON des chatons
#   à mettre dans le site entraide
#   
# 


with open(listfile) as json_file:
    chatons = json.load(json_file)

sslctx=ssl.SSLContext()

listentraide = []

for chaton in chatons['chatons']:
    try:
        name = chaton['name']
    except ValueError:
        print("problem with %s" % name)
    try:
        url = url_normalize(chaton['url'])
    except ValueError :
        print("problem url_normalize with %s" % name)
    try:
        resp = req.urlopen(url,context=sslctx)
    except ValueError:
        print("problem urlopen with %s" % name)
    try:
        data = json.loads(resp.read().decode("utf-8"))
    except ValueError:
        print("problem jsonloads with %s" % name)
    try:
        listentraide += data['nodes']
    except ValueError:
        print("problem listentraide with %s" % name)

nodes = { "nodes": listentraide }

if debug:
    print(json.dumps(nodes))

with open(jsonOutput, 'w') as outfile:
        json.dump(nodes, outfile)

exit(0)
